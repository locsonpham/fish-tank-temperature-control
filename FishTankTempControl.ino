#include <ESP8266WiFi.h>
#include <Ticker.h>
 
const char* ssid = "NgheuHapSa";
const char* password = "Dk123456";
 
//int ledPin = LED_BUILTIN;
WiFiServer server(80);

#include <OneWire.h>
#include <DallasTemperature.h>

#define ON LOW
#define OFF HIGH

// RELAY
int relayPin = 16;
int relayState = OFF;

// DS18B20 Temperature Sensor
// GPIO where the DS18B20 is connected to
const int oneWireBus = 2;     

// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(oneWireBus);

// Pass our oneWire reference to Dallas Temperature sensor 
DallasTemperature sensors(&oneWire);

float temperature = 0;

float temperatureRead(void) {
  sensors.requestTemperatures(); 
  float temperatureC = sensors.getTempCByIndex(0);
  return temperatureC;
}

// TimerX
Ticker blinker;

void temperatureControl(void) {
  sensors.requestTemperatures();
  temperature = sensors.getTempCByIndex(0);

  if (temperature >= 27) {
    relayState = ON;
    digitalWrite(relayPin, relayState);
  }

  if (temperature <= 25) {
    relayState = OFF;
    digitalWrite(relayPin, relayState);
  }
}

void setup() {
  Serial.begin(115200);
  delay(10);
 
  pinMode(relayPin, OUTPUT);
  digitalWrite(relayPin, relayState);
  
  // Start the DS18B20 sensor
  sensors.begin();

  //Initialize timerX every 1s
//  blinker.attach(10, temperatureControl);
 
  // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
 
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
 
  // Start the server
  server.begin();
  Serial.println("Server started");
 
  // Print the IP address
  Serial.print("Use this URL to connect: ");
  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.println("/");
 
}
 
void loop() {
  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    if (millis() % 5000 == 0) temperatureControl();
    return;
  }
 
  // Wait until the client sends some data
  Serial.println("new client");
  while(!client.available()){
    if (millis() % 5000 == 0) temperatureControl();
    delay(1);
  }

  if (millis() % 5000 == 0) temperatureControl();
 
  // Read the first line of the request
  String request = client.readStringUntil('\r');
  Serial.println(request);
  client.flush();
 
  // Match the request
  if (request.indexOf("/LED=ON") != -1)  {
    relayState = ON;
  }
  if (request.indexOf("/LED=OFF") != -1)  {
    relayState = OFF;
  }
 
// Set ledPin according to the request
//digitalWrite(relayPin, relayState);
 
  // Return the response
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println(""); //  do not forget this one
  client.println("<!DOCTYPE HTML>");
  client.println("<html>");
 
  client.print("Temperature: ");
  client.print(temperature);

  client.println("<br><br>");
  client.print("Relay: ");
  if(relayState == ON) {
    digitalWrite(relayPin, relayState);
    client.print("ON");
  } else {
    digitalWrite(relayPin, relayState);
    client.print("OFF");
  }
  client.println("<br><br>");
  client.println("<a href=\"/LED=ON\"\"><button>Turn On </button></a>");
  client.println("<a href=\"/LED=OFF\"\"><button>Turn Off </button></a><br />");  
  client.println("</html>");
 
  delay(1);
  Serial.println("Client disonnected");
  Serial.println("");
 
}
 
